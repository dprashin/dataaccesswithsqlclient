INSERT INTO Power(Name, Description) Values (
'Thunder', 'Allows a hero to summon destructive thunders')

INSERT INTO Power(Name, Description) Values (
'Web', 'Allows a hero create webs used for fighting enemies and movement')

INSERT INTO Power(Name, Description) Values (
'Nightvision', 'Allows a hero to be more powerful at night')

INSERT INTO Power(Name, Description) Values (
'SuperStrength', 'Hero is unnaturally stronk')


INSERT INTO Superhero_Power(Id_Superhero, Id_Power) Values (
1,3);

INSERT INTO Superhero_Power(Id_Superhero, Id_Power) Values (
1,4);

INSERT INTO Superhero_Power(Id_Superhero, Id_Power) Values (
2,2);

INSERT INTO Superhero_Power(Id_Superhero, Id_Power) Values (
3,1);

INSERT INTO Superhero_Power(Id_Superhero, Id_Power) Values (
3,4);


