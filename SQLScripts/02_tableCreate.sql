CREATE TABLE Superhero (
Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
Name varchar(255),
Alias varchar(255),
Origin varchar(255)
);

CREATE TABLE Assistant (
Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
Name varchar(255) UNIQUE
);

CREATE TABLE Power (
Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
Name varchar(255),
Description varchar(255)
);
