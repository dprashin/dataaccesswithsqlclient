/*	create table
	alter table 1 (fk)
	alter table 2 (fk)
	alter new table (pk consisting of both fk's) */

CREATE TABLE Superhero_Power (
Id_Superhero INT  FOREIGN KEY REFERENCES Superhero(Id),
Id_Power INT FOREIGN KEY REFERENCES Power(Id),
PRIMARY KEY (Id_Superhero, Id_Power)
)