﻿using DataAccessApplication.Models;
using DataAccessApplication.Repositories;
using System;
using System.Collections.Generic;

namespace DataAccessApplication {
    class Program {
        static void Main(string[] args) {

            ICustomerRepository repository = new CustomerRepository();

            var customer = repository.GetCustomer(34);
            PrintGetCustomer(customer);

            var customerList = repository.GetAllCustomers();
            PrintGetAllCustomers(customerList);

            var customerManoj = repository.GetCustomer("Dominiq");
            TestGetCustomerByName(customerManoj);

            var customersUsingLimitOffset = repository.GetCustomersOffsetLimit(10, 10);
            PrintGetCustomersOffsetLimit(customersUsingLimitOffset);

            var customerAdded = repository.AddNewCustomer(new Customer()
            {
                FirstName = "Mister",
                LastName = "Bombastic",
                Country = "Fairyland",
                PostalCode = "666",
                Phone = "123 456 789",
                Email = "bombastic@gmail.com"
            });
            
            PrintAddNewCustomer(customerAdded);

            var customerUpdated = repository.UpdateCustomer(new Customer()
            {
                CustomerId = 60,
                LastName = "DratDrat"
            });

            PrintUpdateCustomer(customerUpdated);

            var customerCountryList = repository.GetCustomersByCountry();
            PrintCustomersByCountry(customerCountryList);

            var highSpendingCustomerList = repository.GetHighSpendingCustomers();
            PrintHighSpendingCustomers(highSpendingCustomerList);

            var favouriteGenreOfACustomer = repository.GetFavouriteGenreOfACustomer(12);
            PrintGetFavouriteGenreOfACustomer(favouriteGenreOfACustomer);
            Console.WriteLine();
        }

        private static void PrintGetFavouriteGenreOfACustomer(List<CustomerGenre> favouriteGenreOfACustomer) {
            Console.WriteLine("----------------- Favourite Genere of Customer --------------------------------------");
            foreach (CustomerGenre customerGenre in favouriteGenreOfACustomer) {
                Console.WriteLine($"--- {customerGenre.GenreName} {customerGenre.GenreTracksCount} ---");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Prints a customer.
        /// </summary>
        /// <param name="customer">A Customer object.</param>
        static void PrintGetCustomer(Customer customer) {
            Console.WriteLine($"----------------- Customer {customer.CustomerId}: ------------------------------------------------------ ");
            Console.WriteLine($"--- {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}  ---");
            Console.WriteLine();
        }
        /// <summary>
        /// Prints customers.
        /// </summary>
        /// <param name="customers">A list of customers.</param>
        static void PrintGetAllCustomers(List<Customer> customers) {
            Console.WriteLine("----------------- All customers -------------------------------------------------------");
            foreach (Customer customer in customers) {
                Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}  ---");
            }
            Console.WriteLine();

        }
        /// <summary>
        /// Prints customer.
        /// </summary>
        /// <param name="customer">A Customer object.</param>
        static void TestGetCustomerByName(Customer customer) {
            Console.WriteLine($"----------------- Customer: {customer.FirstName} {customer.LastName} ------------------------------------------------------ ");
            Console.WriteLine($"--- {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}  ---");
            Console.WriteLine();
        }
        /// <summary>
        /// Print customers.
        /// </summary>
        /// <param name="customers">A customer list.</param>
        static void PrintGetCustomersOffsetLimit(List<Customer> customers) {
            Console.WriteLine("----------------- Get Customers using LIMIT and OFFSET ----------------------------------");
            foreach (Customer customer in customers) {
                Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}  ---");
            }
            Console.WriteLine();
        }
        /// <summary>
        /// Prints boolean value.
        /// </summary>
        /// <param name="customerAdded">A boolean value.</param>
        private static void PrintAddNewCustomer(bool customerAdded) {
            Console.WriteLine("----------------- Add new customer -------------------------------------------------------");
            Console.WriteLine($"Customer added: "+ customerAdded);
            Console.WriteLine();

        }
        /// <summary>
        /// Prints boolean value.
        /// </summary>
        /// <param name="customerUpdated">A boolean value</param>
        private static void PrintUpdateCustomer(bool customerUpdated) {
            Console.WriteLine("----------------- Update customer --------------------------------------------------------");
            Console.WriteLine($"Customer updated: " + customerUpdated);
            Console.WriteLine();
        }
        /// <summary>
        /// Prints customers.
        /// </summary>
        /// <param name="customerCountryList">A list of CustomerCountry object</param>
        private static void PrintCustomersByCountry(List<CustomerCountry> customerCountryList) {
            Console.WriteLine("----------------- Customers by country ----------------------------------------------------");
            foreach (CustomerCountry customerCountry in customerCountryList) {
                Console.WriteLine($"--- {customerCountry.Country} {customerCountry.NumberOfCustomers} ---");
            }
            Console.WriteLine();
        }
        /// <summary>
        /// Prints customers.
        /// </summary>
        /// <param name="highSpendingCustomerList">A list of CustomerSpender object.</param>
        private static void PrintHighSpendingCustomers(List<CustomerSpender> highSpendingCustomerList) {
            Console.WriteLine("----------------- High spending customers -------------------------------------------------");
            foreach (CustomerSpender customer in highSpendingCustomerList) {
                Console.WriteLine($"--- {customer.CustomerId} {customer.Total} ---");
            }
            Console.WriteLine();
        }
    }
}
