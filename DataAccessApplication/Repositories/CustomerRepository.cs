﻿using DataAccessApplication.Models;
using DataAccessApplication.Utilities;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessApplication.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Adds a new customer.
        /// </summary>
        /// <param name="customer">A Customer object.</param>
        /// <returns>A boolean value.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
            }
            return success;
        }
        /// <summary>
        /// Delets a customer.
        /// </summary>
        /// <param name="id">An Id of a customer.</param>
        /// <returns>A boolean value.</returns>
        public bool DeleteCustomer(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrives all customers.
        /// </summary>
        /// <returns>A list of Customer object.</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = ReaderHelper.CheckStringFieldForNull(reader, 3);
                                customer.PostalCode = ReaderHelper.CheckStringFieldForNull(reader, 4);
                                customer.Phone = ReaderHelper.CheckStringFieldForNull(reader, 5);
                                customer.Email = ReaderHelper.CheckStringFieldForNull(reader, 6);
                                custList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
            }
            return custList;
        }
        /// <summary>
        /// Retrives a customer by id.
        /// </summary>
        /// <param name="id">An id of a customer.</param>
        /// <returns>A Customer object.</returns>
        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName= reader.GetString(2);
                                customer.Country = ReaderHelper.CheckStringFieldForNull(reader, 3);
                                customer.PostalCode = ReaderHelper.CheckStringFieldForNull(reader, 4);
                                customer.Phone = ReaderHelper.CheckStringFieldForNull(reader, 5);
                                customer.Email = ReaderHelper.CheckStringFieldForNull(reader, 6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to error
            }
            return customer;
        }
        /// <summary>
        /// Retrives a customer by name.
        /// </summary>
        /// <param name="name">Name of the customer.</param>
        /// <returns>A Customer object.</returns>
        public Customer GetCustomer(string name)
        {
            Customer customer = new Customer();
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
                           FROM Customer
                           WHERE FirstName LIKE @FirstName + '%'";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", name);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = ReaderHelper.CheckStringFieldForNull(reader, 3);
                                customer.PostalCode = ReaderHelper.CheckStringFieldForNull(reader, 4);
                                customer.Phone = ReaderHelper.CheckStringFieldForNull(reader, 5);
                                customer.Email = ReaderHelper.CheckStringFieldForNull(reader, 6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to error
            }
            return customer;
        }
        /// <summary>
        /// Retrives customers by defined OFFSET and LIMIT.
        /// </summary>
        /// <param name="offset">Number of entries/records which are discarded from the top. </param>
        /// <param name="limit">Number of entries to show.</param>
        /// <returns>A list of Customers.</returns>
        public List<Customer> GetCustomersOffsetLimit(int offset, int limit)
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                $"ORDER BY CustomerId OFFSET {offset} ROWS FETCH FIRST {limit} ROWS ONLY; ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = ReaderHelper.CheckStringFieldForNull(reader, 3);
                                customer.PostalCode = ReaderHelper.CheckStringFieldForNull(reader, 4);
                                customer.Phone = ReaderHelper.CheckStringFieldForNull(reader, 5);
                                customer.Email = ReaderHelper.CheckStringFieldForNull(reader, 6);
                                custList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
            }
            return custList;
        }
        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="customer">A customer to be updated.</param>
        /// <returns>A boolean value.</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET LastName = @LastName WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
            }
            return success;
        }

        /// <summary>
        /// Returns the number of customers in each country.
        /// </summary>
        /// <returns> A list of CustomerCountry objects.</returns>
        public List<CustomerCountry> GetCustomersByCountry() {
            List<CustomerCountry> custCountryList = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(CustomerId) AS 'Number of customers' FROM Customer GROUP BY Country ORDER BY Country DESC";
            try {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring())) {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn)) {
                        using (SqlDataReader reader = cmd.ExecuteReader()) {
                            while (reader.Read()) {
                                CustomerCountry customerCountry = new CustomerCountry();
                                customerCountry.Country = reader.GetString(0);
                                customerCountry.NumberOfCustomers = reader.GetInt32(1);
                                custCountryList.Add(customerCountry);
                            }
                        }
                    }
                }
            } catch (SqlException ex) {
                // Log to console
            }
            return custCountryList;
        }

        /// <summary>
        /// Returns the high spending customers.
        ///
        /// </summary>
        /// <returns> A list of CustomerCountry objects.</returns>
        public List<CustomerSpender> GetHighSpendingCustomers() {
            List<CustomerSpender> cusSpenderList = new List<CustomerSpender>();
            string sql = "SELECT CustomerId, SUM(Total) AS total_spendings FROM Invoice GROUP BY CustomerId ORDER BY total_spendings DESC";
            try {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring())) {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn)) {
                        using (SqlDataReader reader = cmd.ExecuteReader()) {
                            while (reader.Read()) {
                                CustomerSpender customerSpender = new CustomerSpender();
                                customerSpender.CustomerId = reader.GetInt32(0);
                                customerSpender.Total = reader.GetDecimal(1);
                                cusSpenderList.Add(customerSpender);
                            }
                        }
                    }
                }
            } catch (SqlException ex) {
                //
            }
            return cusSpenderList;
        }
        /// <summary>
        /// Returns a favourite genre of a customer.
        /// </summary>
        /// <param name="customerId">An id of a customer.</param>
        /// <returns>Favourite genre/genres.</returns>
        public List<CustomerGenre> GetFavouriteGenreOfACustomer(int customerId)
        {
            List<CustomerGenre> customerGenreList = new List<CustomerGenre>();
            string sql = "SELECT Customer.CustomerId, Genre.Name, COUNT(Genre.Name) AS fav_genre INTO #TempTable FROM Invoice " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                "WHERE Customer.CustomerId = 12 " +
                "GROUP BY Customer.CustomerId, Genre.Name " +
                "SELECT Name, fav_genre AS TopGenre FROM #TempTable " +
                "WHERE fav_genre = (SELECT MAX(fav_genre) " +
                "FROM #TempTable) " +
                "DROP TABLE #TempTable";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new CustomerGenre();
                                customerGenre.GenreName = reader.GetString(0);
                                customerGenre.GenreTracksCount = reader.GetInt32(1);
                                customerGenreList.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                //
            }
            return customerGenreList;
        }

    }
}
