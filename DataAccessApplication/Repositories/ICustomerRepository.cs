﻿using DataAccessApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessApplication.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(int id);
        public Customer GetCustomer(string name);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetCustomersOffsetLimit(int offset, int limit);

        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public bool DeleteCustomer(int id);
        public List<CustomerCountry> GetCustomersByCountry();
        public List<CustomerSpender> GetHighSpendingCustomers();
        public List<CustomerGenre> GetFavouriteGenreOfACustomer(int customerId);
    }
}
