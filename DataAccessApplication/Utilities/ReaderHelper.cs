﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessApplication.Utilities
{
    public class ReaderHelper
    {
        /// <summary>
        /// Replaces Null value by empty string.
        /// </summary>
        /// <param name="reader">SqlDataReader object </param>
        /// <param name="index">Specific index</param>
        /// <returns>An empty string.</returns>
        public static string CheckStringFieldForNull(SqlDataReader reader, int index)
        {
            if (reader.IsDBNull(index))
                return "";
            else 
                return reader.GetString(index); 
        }
    }
}
